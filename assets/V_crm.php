<?php include('V_header.php') ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="Igniter/C_users" class="tip-bottom">Clientes</a> <a href="#" class="current">Datos Personales</a> </div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Cliente</h5>
          </div>
        </div>
        </div>
      </div>
    </div><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>Datos</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Código Cliente</th>
					<th>Email</th>
					<th>Fecha de Alta</th>
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>Extensión</th>
					<th>Teléfono</th>
                </tr>
              </thead>
			  <tbody>
				<tr>
					<td>Código Cliente</td>
					<td>Email</td>
					<td>Fecha de Alta</td>
					<td>Nombre</td>
					<td>Apellidos</td>
					<td>Extensión</td>
					<td>Teléfono</td>
				</tr>
			  </tbody>
            </table>
          </div>
        </div>
        <div class="widget-box">
      </div>
    </div>
  </div>
    </div>
  </div>
</div>
<div class="row-fluid">
  <div id="footer" class="span12"></div>
</div>
<script src="/Igniter/assets/js/excanvas.min.js"></script> 
<script src="/Igniter/assets/js/jquery.min.js"></script> 
<script src="/Igniter/assets/js/jquery.ui.custom.js"></script>
<script src="/Igniter/assets/js/bootstrap.min.js"></script> 
<script src="/Igniter/assets/js/jquery.flot.min.js"></script> 
<script src="/Igniter/assets/js/jquery.flot.resize.min.js"></script> 
<script src="/Igniter/assets/js/jquery.peity.min.js"></script> 
<script src="/Igniter/assets/js/fullcalendar.min.js"></script> 
<script src="/Igniter/assets/js/maruti.js"></script> 
<script src="/Igniter/assets/js/maruti.dashboard.js"></script> 
<script src="/Igniter/assets/js/maruti.chat.js"></script>
<script src="/Igniter/assets/js/login.labocall.js"></script> 
 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>