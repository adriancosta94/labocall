<div class="row-fluid">
  <div id="footer" class="span12">Labocall</div>
</div>

<script src="/Igniter/assets/js/bootstrap-colorpicker.js"></script>
<script src="/Igniter/assets/js/bootstrap-datepicker.js"></script>
<script src="/Igniter/assets/js/bootstrap.js"></script>
<script src="/Igniter/assets/js/bootstrap.min.js"></script>
<script src="/Igniter/assets/js/excanvas.min.js"></script>
<script src="/Igniter/assets/js/fullcalendar.min.js"></script>
<script src="/Igniter/assets/js/jquery.dataTables.min.js"></script>
<script src="/Igniter/assets/js/jquery.flot.min.js"></script>
<script src="/Igniter/assets/js/jquery.flot.pie.min.js"></script>
<script src="/Igniter/assets/js/jquery.flot.resize.min.js"></script>
<script src="/Igniter/assets/js/jquery.gritter.min.js"></script>
<script src="/Igniter/assets/js/jquery.min.js"></script>
<script src="/Igniter/assets/js/jquery.peity.min.js"></script>
<script src="/Igniter/assets/js/jquery.ui.custom.js"></script>
<script src="/Igniter/assets/js/jquery.uniform.js"></script>
<script src="/Igniter/assets/js/jquery.validate.js"></script>
<script src="/Igniter/assets/js/jquery.wizard.js"></script>
<script src="/Igniter/assets/js/login.labocall.js"></script>
<script src="/Igniter/assets/js/maruti.calendar.js"></script>
<script src="/Igniter/assets/js/maruti.charts.js"></script>
<script src="/Igniter/assets/js/maruti.chat.js"></script>
<script src="/Igniter/assets/js/maruti.dashboard.js"></script>
<script src="/Igniter/assets/js/maruti.form_common.js"></script>
<script src="/Igniter/assets/js/maruti.form_validation.js"></script>
<script src="/Igniter/assets/js/maruti.interface.js"></script>
<script src="/Igniter/assets/js/maruti.js"></script>
<script src="/Igniter/assets/js/maruti.login.js"></script>
<script src="/Igniter/assets/js/maruti.popover.js"></script>
<script src="/Igniter/assets/js/maruti.tables.js"></script>
<script src="/Igniter/assets/js/maruti.wizard.js"></script>
<script src="/Igniter/assets/js/npm.js"></script>
<script src="/Igniter/assets/js/select2.min.js"></script>
<script src="/Igniter/assets/js/"></script>
<script src="/Igniter/assets/js/"></script>


<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>