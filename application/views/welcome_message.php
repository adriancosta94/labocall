<!DOCTYPE html>
<html lang="es">

<head>
        <title>Labocall - Index</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="assets/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="assets/css/maruti-login.css" />
</head>
    <body>
        <div id="loginbox">
            <form id="loginform" class="form-vertical" action="dashboard.php" >
				 <div class="control-group normal_text"> <h3><img src="assets/img/logo.png" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on"><i class="glyphicon glyphicon-user icon-user"></i></span><input type="text" class="userform" placeholder="Usuario" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on"><i class="	glyphicon glyphicon-asterisk icon-lock"></i></span><input type="password" class="passform" placeholder="Contraseña" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-inverse" id="to-recover">Recuperar contraseña</a></span>
                </div>
            </form>
					<span class="pull-right"><input type ="submit" value="Login" class="userpass" id="button"/></span>
            <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text">Introduce tu email para recuperar tu contraseña</p>

                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-inverse" id="to-login">&laquo; Volver al login</a></span>
                    <span class="pull-right"><input type="submit" id ="button" value="Recover" /></span>
				</div>
            </form>
        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/maruti.login.js"></script>
	<script src="assets/js/login.labocall.js"></script>
    </body>

</html>
