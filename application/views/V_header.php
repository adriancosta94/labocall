<!DOCTYPE html>
<html lang="es">
<head>
	<title>Labocall</title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel='stylesheet' href='/labocall/assets/css/bootstrap.min.css' />
	<link rel='stylesheet' href='/labocall/assets/css/bootstrap-responsive.min.css' />
	<link rel='stylesheet' href='/labocall/assets/css/fullcalendar.css' />
	<link rel='stylesheet' href='/labocall/assets/css/maruti-style.css' />
	<link rel='stylesheet' href='/labocall/assets/css/maruti-media.css' class='skin-color' />
	<link rel='stylesheet' href='/labocall/assets/css/maruti-media.css' class='skin-color' />
	<link rel="stylesheet" href="/labocall/assets/css/uniform.css" />
	<link rel="stylesheet" href="/labocall/assets/css/select2.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.html">Panel de control</a></h1>
</div>
<!--close-Header-part-->

<!--top-Header-messaages-->
<div class="btn-group rightzero"> <a class="top_message tip-left" title="Manage Files"><i class="icon-file"></i></a> <a class="top_message tip-bottom" title="Manage Users"><i class="icon-user"></i></a> <a class="top_message tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a> <a class="top_message tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> </div>
<!--close-top-Header-messaages-->

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li class="" ><a title="" href="#"><i class="icon icon-user"></i> <span id="username" class="text">admin</span></a></li>
    <li class=" dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Mensajes</span> <span class="label label-important">5</span> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#">Nuevo mensaje</a></li>
        <li><a class="sInbox" title="" href="#">Bandeja de entrada</a></li>
        <li><a class="sOutbox" title="" href="#">Bandeja de salida</a></li>
        <li><a class="sTrash" title="" href="#">Basura</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="/labocall/index.php/C_profile"><i class="icon icon-cog"></i> <span class="text">Ajustes</span></a></li>
    <li class=""><a title="" href="/labocall/index.php"><i class="icon icon-share-alt"></i> <span class="text">Desconectarse</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Panel de control</a><ul>
    <li class="active"><a href="/labocall/index.php/C_users/index"><i class="icon icon-home"></i> <span>Panel de control</span></a> </li>
    <li> <a href="/labocall/index.php/C_calendar/index"><i class="icon icon-signal"></i> <span>Calendario</span></a> </li>
    <li> <a href="/labocall/index.php/C_analytics/index"><i class="icon icon-inbox"></i> <span>Estadisticas</span></a> </li>
    <li><a href="/labocall/index.php/C_crm/index"><i class="icon icon-th"></i> <span>CRM</span></a></li>
  </ul>
</div>
