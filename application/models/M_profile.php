<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_profile extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	function index(){
		echo ("Estas en M_crm");
	}
	function getdata($busqueda){
		foreach ($busqueda->result() as $row){
					$res = array (
						'user' => $row->user,
						'nomuser' => $row->nomuser,
						'suruser' => $row->suruser,
						'tlfuser'  => $row->tlfuser,
						'emailuser' => $row->emailuser,
						'extuser' => $row->extuser,
						'ultima_sesion' => $row->ultima_sesion,
						'imguser' => $row->imguser
					);
		}
		return $res;
	}
	function buscaruser($users){
			//Construcción de la consulta
			$this->db->select('user,nomuser,suruser,tlfuser,emailuser,extuser,ultima_sesion,imguser');
			$this->db->from('users');
			//$this->db->where('user','admin');
			$this->db->where('user', $users['user']);
			$busqueda = $this->db->get();
			if($busqueda->num_rows()>0){
			$res=$this->getdata($busqueda);//Error
					return $res;
			}else{
					return false;
			}
	}
}
