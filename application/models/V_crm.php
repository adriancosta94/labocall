<?php include('V_header.php') ?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="Igniter/C_users" class="tip-bottom">Clientes</a> <a href="#" class="current">Datos Personales</a> </div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Búsqueda de Clientes</h5>
          </div>
			<div>
				Código Cliente:<label><input id="cli_cod" type="text" value=""/></label>
				Nombre:<label><input type="text" id="cli_nom" value=""/></label>
				Apellidos:<label><input type="text" id="cli_sur" value=""/></label><br>
				<br>
				Email:<label><input type="text" id="cli_email" value=""/></label>
				Tlf:<label><input type="text" id="car_tlf" value=""/></label>
				Extensión:<label><input type="text" id="car_ext" value=""/></label>
				Buzón:<label><input type="text" id="car_buzon" value=""/></label>
				Fecha de Alta:<label><input type="text" id="cli_fecha_alta" value=""/></label>
				<br>
				Inactivo:<input type="checkbox" id="cli_activo"/>
				<br>
			</div>
			<div class="form-actions">
					<input id="searchcli" class="btn btn-primary" type="submit" value="Buscar" />
					<br><hr>
					<button type="submit" id="insertcli" class="btn btn-success">Dar de alta</button>
				<div id="status"></div>
			</div>
        </div>
      </div>
    </div>
  </div>
  <hr>
<div id="content">
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>Datos de Clientes</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Código Cliente</th>
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>Email</th>
					<th>Extensión</th>
					<th>Fecha de Alta</th>
					<th>Teléfono</th>
					<th>Buzón</th>
                </tr>
              </thead>
			  <tbody>
				<tr>
					<?php
						echo $res_model->cli_cod;
					?>
					<td>350023</td>
					<td>Nestor</td>
					<td>Galvañ</td>
					<td>adrian@gmail.com</td>
					<td>3500</td>
					<td>01/01/2017</td>
					<td>615186214</td>
					<td>Buzón</td>
					<td><button type="submit" class="btn btn-info">Edit</button></td>
				</tr>
			</tbody>
            </table>
          </div>
        </div>
        <div class="widget-box">
      </div>
    </div>
  </div>
</div>
</div>
<?php include('V_footer.php') ?>