<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_calendar extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_calendar');
	}
	public function index(){
		$this->load->view('V_calendar');
	}
}
