<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_crm extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_crm');
	}
	public function index(){
		$this->load->view('V_crm');
	}
	public function buscarcli(){
		$cli_cod=$this->input->post("cli_cod");
		$cli_nom=$this->input->post("cli_nom");
		$cli_sur=$this->input->post("cli_sur");
		$car_tlf=$this->input->post("car_tlf");
		$car_ext=$this->input->post("car_ext");
		$car_buzon=$this->input->post("car_buzon");
		$cli_email=$this->input->post("cli_email");
		$cli_fecha_alta=$this->input->post("cli_fecha_alta");
		$cli_activo=$this->input->post("cli_activo");

		$cli_data = array (
		'cli_cod' => $cli_cod,
		'cli_nom' => $cli_nom,
		'cli_sur' => $cli_sur,
		'cli_email' => $cli_email,
		'cli_activo' => $cli_activo,
		'cli_fecha_alta' => $cli_fecha_alta
		);
		$car_data = array (
		'car_codcli' => $cli_cod,
		'car_tlf' => $car_tlf,
		'car_ext' => $car_ext,
		'car_buzon' => $car_buzon
		);

		if($data['res']=$this->M_crm->buscarcli($cli_data,$car_data))
		{
			echo json_encode($data['res']);
		}else{
			return false;
		}
	}
	public function insertcli(){
		$cli_cod=$this->input->post("cli_cod");
		$cli_nom=$this->input->post("cli_nom");
		$cli_sur=$this->input->post("cli_sur");
		$car_tlf=$this->input->post("car_tlf");
		$car_ext=$this->input->post("car_ext");
		$car_buzon=$this->input->post("car_buzon");
		$cli_email=$this->input->post("cli_email");
		$cli_fecha_alta=date("Y-m-d");
		$cli_activo = 1;
		$cli_data = array (
		'cli_cod' => $cli_cod,
		'cli_nom' => $cli_nom,
		'cli_sur' => $cli_sur,
		'cli_email' => $cli_email,
		'cli_fecha_alta' => $cli_fecha_alta,
		'cli_activo' => $cli_activo
		);
		$car_data = array (
		'car_codcli' => $cli_cod,
		'car_tlf' => $car_tlf,
		'car_ext' => $car_ext,
		'car_buzon' => $car_buzon,
		);
		if($data['res']= $this->M_crm->buscarcli($cli_data,$car_data))
		{
			/*$error = "Este cliente ya existe";
			$disponibilidadID= "El último ID disponible es ".$data['ultimoid'];
			$data['error']=$error;
			$data['ultimoid']=$this->M_crm->getultimoid($cli_data);*/
			echo json_encode($data['res']);
		}else{
			$this->M_crm->insertcli($cli_data,$car_data);
		}

	}
	public function editcli(){
		$cli_cod=$this->input->post("cli_cod");
		$cli_nom=$this->input->post("cli_nom");
		$cli_sur=$this->input->post("cli_sur");
		$car_tlf=$this->input->post("car_tlf");
		$car_ext=$this->input->post("car_ext");
		$car_buzon=$this->input->post("car_buzon");
		$cli_email=$this->input->post("cli_email");
		$cli_fecha_alta=$this->input->post("cli_fecha_alta");
		$cli_activo=$this->input->post("cli_activo");

		$cli_data = array (
		'cli_cod' => $cli_cod,
		'cli_nom' => $cli_nom,
		'cli_sur' => $cli_sur,
		'cli_email' => $cli_email,
		'cli_activo' => $cli_activo,
		'cli_fecha_alta' => $cli_fecha_alta
		);
		$car_data = array (
		'car_codcli' => $cli_cod,
		'car_tlf' => $car_tlf,
		'car_ext' => $car_ext,
		'car_buzon' => $car_buzon
		);

		if($data['res']=$this->M_crm->editcli($cli_data,$car_data))
		{
			return true;
		}else{
			return false;
		}
	}
}
