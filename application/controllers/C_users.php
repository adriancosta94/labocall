<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_users extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_users');
	}
	function index(){
		session_start();
		$_SESSION["user"]=$this->uri->segment(3);
		$array['user'] = $this->uri->segment(3);
		$this->load->view('V_header',$array);
	}
	function getlogin(){
		$user=$this->input->post("user");
		$_SESSION['user']=$this->input->post("user");
		$pass=$this->input->post("pass");
		if($this->verify($user,$pass)){
			if($this->M_users->loginComparation($user,$pass)){
				$_SESSION['user'] = $user;
				echo "TRUE";

			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function verify($user,$pass){
		if($user != ""){
			return true;
		}else if($pass != ""){
			return true;
		}else{
			return false;
		}
	}
}
