<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_analytics extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_analytics');
	}
	public function index(){
		$this->load->view('V_analytics');
	}
}
