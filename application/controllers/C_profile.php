<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_profile extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_profile');
	}
	public function index(){
		$this->load->view('V_profile');
	}
	public function changeprofile(){
		$user=$this->input->post("user");
		$nomuser=$this->input->post("nomuser");
		$suruser=$this->input->post("suruser");
		$tlfuser=$this->input->post("tlfuser");
		$extuser=$this->input->post("extuser");
		$emailuser=$this->input->post("emailuser");
		$ultima_sesion=$this->input->post("ultima_sesion");
		$imguser=$this->input->post("imguser");

		$users_data = array (
		'user' => $user,
		'nomuser' => $nomuser,
		'suruser' => $suruser,
		'tlfuser' => $tlfuser,
		'extuser' => $extuser,
		'emailuser' => $emailuser,
		'ultima_sesion' => $ultima_sesion,
		'imguser' => $imguser,
		);
		$this->M_profile->changeprofile($users_data);
	}
	public function buscaruser(){
		$user= $this->input->post("user");
		$users = array (
			'user' => $user
		);
		if($data['res']=$this->M_profile->buscaruser($users))
		{
			echo json_encode($data['res']);
		}else{
			return false;
		}
	}
}
